# Useful Kubernetes Things

## bashrc-kube

provides a single bash function: `kimages`

```sh

(venvX)* [[k8s-prod:abc]]
gladiatr@dev-wrk
[~/git/useful-things]
:# kimages
abc-memcache (namespace=abc)
        memcached:1.5-alpine
abc-proc (namespace=abc)
        registry.gitlab.com/revsys/abc-proc:v1.3.59
abc-proc (namespace=abc)
        registry.gitlab.com/revsys/abc-proc:v1.3.59
abc-ui (namespace=abc)
        registry.gitlab.com/revsys/abc-ui:v0.0.49
        nginx:1.12.1-alpine
abc-ui (namespace=abc)
        registry.gitlab.com/revsys/abc-ui:v0.0.48
        nginx:1.12.1-alpine

(venvX)* [[k8s-prod:abc]]
gladiatr@dev-wrk
:# kimages abc-ui
abc-ui (namespace=abc)
        registry.gitlab.com/revsys/abc-ui:v0.0.49
        nginx:1.12.1-alpine
abc-ui (namespace=abc)
        registry.gitlab.com/revsys/abc-ui:v0.0.48
        nginx:1.12.1-alpine


```

using **all** as the argument will display all queryable pods running across the 
cluster.

### usage

`bashrc-kube` can be placed anywhere. From `~/.bashrc` add the line `. /path/to/bashrc-kube`.


